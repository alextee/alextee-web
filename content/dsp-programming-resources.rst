DSP Programming Resources
#########################

:date: 2019-07-09 23:15
:tags: audio, dsp
:category: programming
:slug: dsp-programming-resources
:authors: Alexandros Theodotou
:summary: Resources for DSP programming.

===================================================================================== ============================================================================================
Name                                                                                  URL
===================================================================================== ============================================================================================
JOS @ Stanford                                                                        https://ccrma.stanford.edu/~jos
DAFx-15 Proceedings                                                                   https://www.ntnu.edu/dafx15/proceedings
The Quest For The Perfect Resampler                                                   https://www.mp3-tech.org/programmer/docs/resampler.pdf
VAFilterDesign                                                                        https://www.native-instruments.com/fileadmin/ni_media/downloads/pdf/VAFilterDesign_2.1.0.pdf
The Ardour DAW – Latency Compensation and Anywhere-to-Anywhere Signal Routing Systems https://gareus.org/misc/thesis-p8/2017-12-Gareus-Lat.pdf
Hack audio                                                                            https://www.hackaudio.com/ *(contains trackers - only access with protection)*
Real-time audio programming 101: time waits for nothing                               http://www.rossbencina.com/code/real-time-audio-programming-101-time-waits-for-nothing
MIDI Event Table                                                                      http://www.onicos.com/staff/iz/formats/midi-event.html
===================================================================================== ============================================================================================
