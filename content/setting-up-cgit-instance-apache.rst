Setting Up a CGit Instance On Apache (CentOS 7)
###############################################

:date: 2019-06-29 15:04
:tags: apache, web, server, git
:category: server
:slug: setting-up-cgit-instance-apache
:authors: Alexandros Theodotou
:summary: Basic steps to get a cgit instance up and running on
          CentOS 7 using Apache.

The following steps assume that our domain is already set
up at cgit.myserver.org.

First, install cgit and highlight

.. code-block:: bash

  sudo yum install cgit highlight

Create the repos at ``/srv/git`` (can be any path)

.. code-block:: bash

  mkdir -p /srv/git
  cd /srv/git

  # clone an existing repository to become a remote for it
  git clone --bare https://git.savannah.nongnu.org/git/zrythm.git

Add the following to the cgit conf file at ``/etc/cgitrc`` to show our repo

.. code-block:: bash

  repo.url=zrythm
  repo.path=/srv/git/zrythm.git
  repo.desc=My repo description
  repo.owner=me@myserver.org

Follow the steps `here <https://www.creang.com/howtoforge/howto_set_up_git_over_https_with_apache_on_ubuntu/>`_
to set up the git user's shell to the ``git-shell`` (path will vary) and home directory
where we want our repos hosted.

Create an Apache configuration at ``/etc/httpd/conf.d/cgit.conf`` or somewhere
that Apache can read

.. code-block:: aconf

  # This section is only needed if you want to redirect http traffic to https.
  <VirtualHost *:80>
    ServerName cgit.myserver.org
    ServerSignature Off

    RewriteEngine on
    RewriteCond %{SERVER_NAME} =cgit.myserver.org
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
  </VirtualHost>

  <VirtualHost *:443>
    SSLEngine on
    ServerName cgit.myserver.org

    # files in /usr/share/cgit will be available from URL /cgit-data
    Alias /cgit-data /usr/share/cgit

    # /cgit page is the cgit CGI
    ScriptAlias /cgit /var/www/cgi-bin/cgit

    # Redirect main page to /cgit
    RewriteEngine on
    RewriteCond %{HTTP_HOST} cgit\.myserver\.org [NC]
    RewriteCond %{REQUEST_URI} ^/$
    Rewriterule ^(.*)$ https://cgit.myserver.org/cgit/ [L,R=301]

    # Allow anonymous https pulling but require authorization for writing
    SetEnv GIT_PROJECT_ROOT /srv/git
    SetEnv GIT_HTTP_EXPORT_ALL
    ScriptAlias /git/ /usr/libexec/git-core/git-http-backend/

    RewriteCond %{QUERY_STRING} service=git-receive-pack [OR]
    RewriteCond %{REQUEST_URI} /git-receive-pack$
    RewriteRule ^/git/ - [E=AUTHREQUIRED:yes]

    <LocationMatch "^/git/">
      Order Deny,Allow
      Deny from env=AUTHREQUIRED

      AuthType Basic
      AuthName "Git Access"
      Require group mygroup
      Satisfy Any
    </LocationMatch>

    <Directory "/usr/share/cgit">
      Require all granted
    </Directory>
  </VirtualHost>

Restart Apache and call certbot to get a certificate for the site

.. code-block:: bash

  sudo service httpd restart
  sudo certbot --apache

Check that Certbot added the following to Apache's ``cgit.conf``

.. code-block:: aconf

  Include /etc/letsencrypt/options-ssl-apache.conf
  SSLCertificateFile /etc/letsencrypt/live/www.myserver.org-0001/cert.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/www.myserver.org-0001/privkey.pem
  SSLCertificateChainFile /etc/letsencrypt/live/www.myserver.org-0001/chain.pem

Done! We can now browse the repo in cgit and clone it using https

.. code-block:: bash

  git clone https://cgit.myserver.org/git/zrythm.git

We will use SSH for writing to the repo. Just create an ``.ssh`` dir in the git
user's home dir and put public keys in ``.ssh/authorized_keys`` (remember to set
permissions correctly)

References
----------
- https://askubuntu.com/questions/788589/how-to-setup-cgit-with-apache2-in-ubuntu
- https://wiki.archlinux.org/index.php/Cgit
- https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-http-backend.html
- https://www.creang.com/howtoforge/howto_set_up_git_over_https_with_apache_on_ubuntu/
