Tools for Tinkering with MIDI Files
###################################

:date: 2019-08-31 16:02
:tags: midi, audio
:category: audio
:slug: tools-for-tinkering-with-midi-files
:authors: Alexandros Theodotou
:summary: Collection of tools and programs for verifying the content
          of MIDI files.

Editors
=======

================ ======================================================= ===============================================
Name             Description                                             URL
================ ======================================================= ===============================================
MidiEditor       Graphical interface to edit, play, and record Midi data https://www.midieditor.org/
================ ======================================================= ===============================================

Command Line Tools
==================

================ ======================================================= ===============================================
Name             Description                                             URL
================ ======================================================= ===============================================
midish           command-line MIDI sequencer and filter                  http://www.midish.org
midicomp         convert SMF MIDI files to and from plain text           https://github.com/markc/midicomp
midiplay         MIDI file player                                        https://github.com/jpcima/midiplay/
================ ======================================================= ===============================================

Libraries
=========

================ ======================================================= ===============================================
Name             Description                                             URL
================ ======================================================= ===============================================
miditk-smf       Python toolkit for working with Standard MIDI files     https://github.com/SpotlightKid/miditk-smf
================ ======================================================= ===============================================


