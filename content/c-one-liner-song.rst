One-Liner Song in C
###################

:date: 2019-05-28 06:22
:tags: programming, audio, c
:category: audio
:slug: c-one-liner-song
:authors: Alexandros Theodotou
:summary: An one-liner song written in C

A friend just showed me this cool way to generate a full song using 1 line
of C code.

.. code-block:: c

  main(t){for(;;t++){putchar((t*(t&t>>12)*8/11025)|0,((t&16)/8-1)*(t*(t^15)+t+127));}}

Dump this in a ``main.c``, and compile it

.. code-block:: bash

  echo "main(t){for(;;t++){putchar((t*(t&t>>12)*8/11025)|0,((t&16)/8-1)*(t*(t^15)+t+127));}}" > main.c
  gcc main.c -o song

One way to play it is to pipe it to ``aplay`` (needs ALSA)

.. code-block:: bash

  ./song | aplay -r 44100

Another way is to dump it into ``file.raw`` and use ``ffplay`` (needs FFMPEG)

.. code-block:: bash

  ./song > file.raw
  ffplay -f u8 -acodec pcm_u8 -ar 44100 file.raw

Remember to use ``Ctrl-C`` to stop it since it loops infintely.

Enjoy!
