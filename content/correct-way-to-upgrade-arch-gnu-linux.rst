The Correct Way To Upgrade Arch GNU/Linux
#########################################

:date: 2019-05-28 09:36
:tags: parabola, gnu, linux, distro
:category: system
:slug: correct-way-to-upgrade-arch-gnu-linux
:authors: Alexandros Theodotou
:summary: The "correct" way to upgrade an Arch GNU/Linux system that works in
          all cases

While upgrading Parabola GNU/Linux (which is based on Arch GNU/Linux) using
the usual ``pacman -Syu``, I was getting the following
error and the upgrade was deadlocked:

.. code-block:: bash

  error: grub2-theme-gnuaxiom: signature from "bill-auger <email redacted>" is unknown
  trust:: File /var/cache/pacman/pkg/grub2-theme-gnuaxiom-1.5-1-any.pkg.tar.xz is corrupted (invalid or corrupted package (PGP signature)).
  Do you want to delete it? [Y/n] y
  error: linux-libre: signature from "bill-auger <email redacted>" is unknown
  trust:: File /var/cache/pacman/pkg/linux-libre-5.1.3_gnu-1-x86_64.pkg.tar.xz is corrupted (invalid or corrupted package (PGP signature)).
  Do you want to delete it? [Y/n] y

I asked in the #parabla freenode channel and apparently "in the special case where
``-Syu`` is blocked on a keyring update, you can ``pacman -Sy *-keyring && pacman -Su``"

So I guess the "correct" way to upgrade the system that works in all cases is
to run

.. code-block:: bash

  pacman -Sy *-keyring --needed && pacman -Su

Here's some explanations I received:

.. code-block:: text

  but what happens if your full upgrade *needs* the keys to be installed first?
  i.e. pacman -Syu tries to update two packages: parabola-keyring and linux-libre
  so, in order to check whether linux-libre is *valid*, you need the new keys
  but in order to get the new keys, you need to finish installing parabola-keyring
  and that requires two passes, which is what -Sy parabola-keyring && -Su does
