Printing the Latest Tag Name from a Remote Git Repository
#########################################################

:date: 2019-07-08 22:07
:tags: git
:category: unix
:slug: printing-latest-tag-name-remote-git-repo
:authors: Alexandros Theodotou
:summary: One-liner to print the name of the latest tag in a remote
          git repository.

During the builds of my websites I use a variable to set the current
version of my program, and that gets used in various places on the
website including download links. I have been doing this manually and
then I thought, what if I just fetch the latest version automatically
during the build?

So here it is:

.. code-block:: bash

  git ls-remote --tags https://<myrepo> | grep -o 'refs/tags/v[0-9]*\.[0-9]*\.[0-9]*' | sort -r | head -n 1 | grep -o '[^\/]*$'

Now when the websites get built, the variable will get set to the
result of this command, which is the latest tag pushed in the
repository (which in my case is the latest relased version).

Note: the command assumes that the version is in the format v0.0.0

References
----------
- https://stackoverflow.com/a/20742976
