Hosting Multi-Language Websites on Apache
#########################################

:date: 2019-05-05 02:44
:tags: apache, web, server, localization, website
:category: server
:slug: hosting-multi-language-websites-apache
:authors: Alexandros Theodotou
:summary: An example config file for hosting multi-language websites on Apache web server.

Let's assume that the document root is /var/www/mysite and our website is
https://www.mysite.org, and that the site has each translated sub-site under
/var/www/mysite/en, /var/www/mysite/de, etc. (so https://www.mysite.org/en,
https://www.mysite.org/de, etc.).

We write a rewrite rule to force a redirect from mysite.org/ to
mysite.org/<language>/, where language is the 2-letter ISO code of
the language, passed by the browser's "Accept-Language" header. We also
write a rewrite rule to force a redirect from mysite.org/index.html to
mysite.org/<language>/index.html.

.. code-block:: aconf

  <IfModule mod_ssl.c>
  <VirtualHost *:443>
  DocumentRoot /var/www/mysite
  ServerName www.mysite.org

  # choose site based on language
  RewriteEngine on
  RewriteCond %{HTTP_HOST} mysite\.org [NC]
  RewriteCond %{REQUEST_URI} ^/$
  RewriteCond %{HTTP:Accept-Language} ^.*(de|es|fr|it|ja|ru|en|zh|pt).*$ [NC]
  Rewriterule ^(.*)$ https://www.mysite.org/%1/ [L,R=301]

  RewriteCond %{HTTP_HOST} mysite\.org [NC]
  RewriteCond %{REQUEST_URI} ^/index\.html$
  RewriteCond %{HTTP:Accept-Language} ^.*(de|es|fr|it|ja|ru|en|zh|pt).*$ [NC]
  Rewriterule ^(.*)$ https://www.mysite.org/%1/index.html [L,R=301]

  # other directives here

  </VirtualHost>
  </IfModule>


Ideas taken from `here <https://www.askapache.com/htaccess/htaccess-mod_rewrite-rewrite-examples/>`_.
