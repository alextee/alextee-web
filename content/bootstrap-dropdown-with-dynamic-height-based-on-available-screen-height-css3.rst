Bootstrap Dropdown With Dynamic Height Based on Available Screen Height (CSS3)
##############################################################################

:date: 2019-05-09 11:41
:tags: web, css, website, bootstrap
:category: website
:slug: bootstrap-dropdown-dynamic-height-available-screen-height-css3
:authors: Alexandros Theodotou
:summary: Example of a Bootstrap dropdown that can readjust its height based on the
          available screen height using CSS3.

On our website we have the following dropdown in our Bootstrap navbar, and we
want to make sure that all of its contents are visible on any screen size.

.. code-block:: html

  <!-- navbar -->
  <div class="collapse navbar-collapse" id="navbarMain">

    <!-- other navbar items -->

    <ul class="navbar-nav navbar-right nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#"
           data-toggle="dropdown" role="button"
           aria-haspopup="true" aria-expanded="false">
           {{ lang_full }} {{ lang_flag }}
        </a>
         <ul class="dropdown-menu dropdown-menu-right scrollable-menu" aria-labelledby="navbarMain">
        {% for key in langs_full.keys() %}
          <li><a class="dropdown-item" href="{{ self_localized(key) }}">{{ lang_flags[key] }} {{ langs_full[key] }}</a></li>
        {% endfor %}
        </ul>
      </li> <!-- /navbar-right dropdown -->
    </ul> <!-- /navbar-right -->
  </div> <!-- /navbar -->

On small screen sizes, using vanilla Bootstrap the dropdown will get cut off when
there is no more space (in height). To mitigate this, we add a CSS class called
`scrollable-menu` to the dropdown-menu `ul`

.. code-block:: html

  <ul class="dropdown-menu dropdown-menu-right scrollable-menu" aria-labelledby="navbarMain">

Then we add CSS rules for how this element will behave

.. code-block:: css

  @media (min-width: 768px) {
    .scrollable-menu {
        height: auto;
        max-height: calc(100vh - 60px);
        overflow-x: hidden;
    }
  }

  @media (max-width: 767px) {
    .scrollable-menu {
      height: auto;
      max-height: calc(100vh - 240px);
      overflow-x: hidden;
    }
  }

According to https://stackoverflow.com/questions/19827236/bootstrap-3-apply-css-on-mobile-view-only, Bootstrap defines anything up to 767px in width as a small device, and this is when
it makes the navbar enter smart phone mode, and since the navbar has different height
in each mode (due to the other navbar menus getting stacked vertically) we want to separate
the rules for each mode. So we use the `CSS @media rule <https://www.w3schools.com/cssref/css3_pr_mediaquery.asp>`_.

.. code-block:: css

  /* rules for desktop sizes */
  @media (min-width: 768px) { }

.. code-block:: css

  /* rules for smart phone sizes */
  @media (max-width: 767px) { }

The next trick is using the `calc` function in CSS3 to calculate the max height we want
our dropdown to have. In this case, we measured the height of our navbar manually in each
screen size and we subtracted that height (plus a few pixels as margin) from `100vh`. `vh`
stands for "percentage of browser screen height", so we use 100% of the full browser screen
height minus the height of the navbar and a little more for extra space.

If the full height of the dropdown does not fit the given height, a scrollbar will appear.

The result should look like this on desktop with large height:

.. image:: {static}/images/dropdown-desktop-large.png
  :alt: Large screen on desktop

Scrollbar appears when height gets small enough:

.. image:: {static}/images/dropdown-desktop-small.png
  :alt: Small screen on desktop

And like this on smaller screen sizes:

.. image:: {static}/images/dropdown-mobile-large.png
  :alt: Large screen on mobile devices

Similarly, scrollbar appears when height is small:

.. image:: {static}/images/dropdown-mobile-small.png
  :alt: Small screen on mobile devices
