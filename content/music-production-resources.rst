Music Production Resources
##########################

:date: 2019-07-09 23:15
:tags: music, sfz, audio
:category: audio
:slug: music-production-resources
:authors: Alexandros Theodotou
:summary: Various freely available music production resources,
          including SFZ/SF2 libraries.

General
=======

This is a list of websites that include a broad range of resources (MIDI clips,
sfz/sf2, samples, etc.).

======================= =============================================
Name                    URL
======================= =============================================
Musical Artifacts       https://musical-artifacts.com/
Freesound               https://freesound.org/browse/
Awesome GNU/Linux Audio https://github.com/nodiscc/awesome-linuxaudio
======================= =============================================

SF2/SFZ Libraries
=================

This is a list of free SFZ/SF2 libraries that I will keep updated as I discover new ones.

If you know of any library under a free license not mentioned here please
shoot me a message. Note that all of these explicitly allow re-distribution,
even of modified versions. Non-free soundfonts or unlicensed ones are not
included.

======================================= ================== ============================================================================================
Name                                    License            URL
======================================= ================== ============================================================================================
Versilian Community Sample Library      CC0 1.0            https://github.com/sgossner/VCSL
RKhive Soundfonts                       CC0 1.0            http://rkhive.com/
GSi soundfonts                          Custom             https://www.genuinesoundware.com/?a=soundfonts
Virtual Playing Orchestra               Various            http://virtualplaying.com/virtual-playing-orchestra
Ethan's SoundFonts                      Custom             http://ethanwiner.com/ewsf2.html
NTONYX SoundFonts                       Custom             http://ntonyx.com/sf_f.htm
sfzinstruments                          CC-BY-4.0          https://github.com/sfzinstruments/
FreePats                                CC0 1.0            http://freepats.zenvoid.org/index.html
Detuned Piano                           CC-BY-SA-3.0       http://download.linuxaudio.org/musical-instrument-libraries/sfz/detuned_piano.tar.7z
Plucked Piano Strings                   CC-BY-SA-3.0       http://download.linuxaudio.org/musical-instrument-libraries/sfz/plucked_piano_strings.tar.7z
The City Piano                          Public domain      http://bigcatinstruments.blogspot.ca/2015/09/all-keyboard-instruments.html
Polyphone Soundfonts                    Various            https://www.polyphone-soundfonts.com/en/soundfonts/search
VS Chamber Orchestra: Community Edition CC0 1.0            https://github.com/sgossner/VSCO-2-CE
Flame Studios                           GPL                http://www.flamestudios.org/free/Soundfonts
Karoryfer Shinyguitar (and others)      CC-BY-4.0          https://www.karoryfer.com/karoryfer-samples/wydawnictwa/shinyguitar
D Smolken's multi-sampled double bass   CC-BY-3.0 Unported http://www.drealm.info/sfz/DSmolken/
Da Real 110 sample library              CC-BY-NC-SA 4.0    https://www.synth.in/2015/06/da-real-110-sample-library.html
======================================= ================== ============================================================================================

TODO: find some from here https://bb.linuxsampler.org/viewforum.php?f=8
