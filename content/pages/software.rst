Software
########

:title: Software
:date: 2019-09-24 15:42
:author: Alexandros Theodotou
:slug: software
:summary: Software I develop or maintain
:template: page
:url: software.html
:save_as: software.html


This is a list of software I develop or maintain. The repositories
can be found at my
`cgit instance <https://git.zrythm.org/cgit/>`_.
All of these are
`free software <https://www.gnu.org/philosophy/free-sw.html>`_
released under
`copyleft <https://en.wikipedia.org/wiki/Copyleft>`_
licenses.

Zrythm
======

`Zrythm <https://www.zrythm.org>`_
is a highly automated and intuitive digital audio workstation
written in C.

gen_invoice
===========

`gen_invoice <https://git.zrythm.org/cgit/gen_invoice/>`_
is a simple command line invoice generator using pandoc.
