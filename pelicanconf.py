#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Alexandros Theodotou'
SITENAME = "alextee's home page"
SITEURL = 'https://www.alextee.org'
# SITEURL = 'file://' + os.getcwd() + '/output'

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = 'en'

# static file paths
STATIC_PATHS = ['static', 'statice/robots.txt']
EXTRA_PATH_METADATA = {
    'static/robots.txt': {'path': 'robots.txt'},
}

# enable plugins
PLUGIN_PATHS = ['../pelican-plugins']
PLUGINS = ['sitemap']

# sitemap plugin conf
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'monthly',
        'pages': 'monthly'
    }
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_ALL_RSS = None
CATEGORY_FEED_RSS = None

SIDEBAR_DIGEST = 'music and software'

MENUITEMS = (('Home', SITEURL + '/index.html'),
             ('Tags', SITEURL + '/tags.html'),
             ('Archives', SITEURL + '/archives.html'))

TEMPLATE_PAGES = {
    'license.html' : 'license.html',
    }

# Blogroll
LINKS = (
    ('source code', 'https://git.zrythm.org/cgit/alextee-web/'),
    )

# Social widget
SOCIAL = (('LiberaPay', 'https://liberapay.com/alextee/', 'fa-liberapay-square'),
          ('Email', 'mailto:alex_at_alextee.org', 'fa-at'),)

DEFAULT_PAGINATION = 4

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'themes/alextee'

DISPLAY_PAGES_ON_MENU = True
